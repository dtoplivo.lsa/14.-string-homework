﻿#include <string>
#include <iostream>
#include <iomanip>


void main()
{
	std::string tring = "1234567890";
	//Выводит саму строку в консоль
	std::cout << "Text: " << tring << "\n"; 
	//Выводит длину строки
	std::cout << "Line length: " << tring.length() << "\n"; 
	//Высчитывает первый символ и выводит его в консоль
	std::cout << "First symbol: " << tring[tring.size() - tring.length()] << "\n"; 
	//Высчитывает последний символ и выводит его в консоль
	std::cout << "Last symbol: " << tring[tring.size() - 1]; 
	std::cin;	
}